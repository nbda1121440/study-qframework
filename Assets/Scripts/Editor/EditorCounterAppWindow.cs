using QFramework;
using QFramework.Example;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorCounterAppWindow : EditorWindow, IController
{
    [MenuItem("QFramework/Example/EditorCounterAppWindow")]
    public static void Open()
    {
        GetWindow<EditorCounterAppWindow>().Show();
    }

    private ICounterAppModel mCounterAppModel;

    private void OnEnable()
    {
        mCounterAppModel = this.GetModel<ICounterAppModel>();
    }

    private void OnDisable()
    {
        mCounterAppModel = null;
    }

    private void OnGUI()
    {
        if (GUILayout.Button("+"))
        {
            this.SendCommand<IncreaseCountCommand>();
        }

        GUILayout.Label(mCounterAppModel.Count.Value.ToString());

        if (GUILayout.Button("-"))
        {
            this.SendCommand<DecreaseCountCommand>();
        }
    }

    public IArchitecture GetArchitecture()
    {
        return CounterApp.Interface;
    }
}
