using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Example
{
    public class SchoolAllPersonCountQuery : AbstractQuery<int>
    {
        protected override int OnDo()
        {
            return this.GetModel<StudentModel>().StudentNames.Count +
                this.GetModel<TeacherModel>().TeacherNames.Count;
        }
    }
}