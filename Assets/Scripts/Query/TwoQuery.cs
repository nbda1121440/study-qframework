using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Example
{
    public class TwoQuery : AbstractQuery<int>
    {
        protected override int OnDo()
        {
            return this.GetModel<ICounterAppModel>().Count.Value * 2;
        }
    }
}