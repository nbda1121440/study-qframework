using QFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Example
{
    /// <summary>
    /// - �߼�
    /// </summary>
    public class DecreaseCountCommand : AbstractCommand
    {
        protected override void OnExecute()
        {
            var model = this.GetModel<ICounterAppModel>();
            model.Count.Value--;
        }
    }
}
