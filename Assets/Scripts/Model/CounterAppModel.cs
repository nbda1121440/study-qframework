using QFramework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Example
{
    public interface ICounterAppModel : IModel
    {
        BindableProperty<int> Count { get; }
    }

    /// <summary>
    /// 定义一个 Model 对象
    /// </summary>
    public class CounterAppModel : AbstractModel, ICounterAppModel
    {
        public BindableProperty<int> Count { get; } = new BindableProperty<int>();

        protected override void OnInit()
        {
            var storage = this.GetUtility<IStorage>();

            // 设置初始值（不触发事件）
            Count.SetValueWithoutEvent(storage.LoadInt(nameof(Count)));

            // 当数据变更时，存储数据
            Count.Register(newCount =>
            {
                storage.SaveInt(nameof(Count), newCount);
            });
        }
    }

}