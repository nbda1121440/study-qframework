using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace QFramework.Example
{
    public class QueryExampleController : MonoBehaviour, IController
    {
        private Button mBtnQuery;
        private Text mText;

        public void Start()
        {
            mBtnQuery = transform.Find("BtnQuery").GetComponent<Button>();
            mText = transform.Find("Text").GetComponent<Text>();

            mBtnQuery.onClick.AddListener(() => {
                int mAllPersonCount = this.SendQuery(new SchoolAllPersonCountQuery());
                mText.text = mAllPersonCount.ToString();
            });
        }

        public IArchitecture GetArchitecture()
        {
            return QueryExampleApp.Interface;
        }
    }
}
