using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace QFramework.Example
{
    public class CounterAppController : MonoBehaviour, IController
    {
        // View
        private Button mBtnAdd;
        private Button mBtnSub;
        private Text mCountText;
        private ToggleGroup toggleGroup;

        // Model
        private ICounterAppModel mModel;

        public void Start()
        {
            // 获取模型
            mModel = this.GetModel<ICounterAppModel>();

            // View 组件获取
            mBtnAdd = transform.Find("BtnAdd").GetComponent<Button>();
            mBtnSub = transform.Find("BtnSub").GetComponent<Button>();
            mCountText = transform.Find("Score").GetComponent<Text>();
            toggleGroup = transform.Find("Toggles").GetComponent<ToggleGroup>();

            mBtnAdd.onClick.AddListener(() => {
                // 交互逻辑
                this.SendCommand<IncreaseCountCommand>();
            });
            mBtnSub.onClick.AddListener(() => {
                // 交互逻辑
                this.SendCommand<DecreaseCountCommand>();
            });
            Toggle[] toggles = toggleGroup.GetComponentsInChildren<Toggle>();
            foreach (Toggle toggle in toggles)
            {
                toggle.onValueChanged.AddListener((value) => { 
                    if (value)
                    {
                        // 说明这个 toggle 被开起来了
                        // Text text = toggle.GetComponentInChildren<Text>();
                        UpdateView();
                    }
                });
            }

            // 表现逻辑
            mModel.Count.RegisterWithInitValue(newCount =>
            {
                UpdateView();
            }).UnRegisterWhenGameObjectDestroyed(gameObject); ;
        }

        public void UpdateView()
        {
            // mCountText.text = mModel.Count.ToString();
            Toggle toggle = toggleGroup.GetFirstActiveToggle();
            Text text = toggle.GetComponentInChildren<Text>();
            if (text.text.Equals("x1"))
            {
                mCountText.text = this.SendQuery(new OneQuery()).ToString();
            }
            else if (text.text.Equals("x2"))
            {
                mCountText.text = this.SendQuery(new TwoQuery()).ToString();
            }
            else if (text.text.Equals("x3"))
            {
                mCountText.text = this.SendQuery(new ThreeQuery()).ToString();
            }
        }

        public IArchitecture GetArchitecture()
        {
            return CounterApp.Interface;
        }

        public void OnDestroy()
        {
            // 将 Model 设置为空
            mModel = null;
        }
    }
}
