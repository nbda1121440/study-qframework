using QFramework;
using QFramework.Example;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Example
{

    /// <summary>
    /// 定义一个架构（提供 MVC、分层、模块管理等）
    /// </summary>
    public class CounterApp : Architecture<CounterApp>
    {
        protected override void Init()
        {
            // 注册 Model
            this.RegisterModel<ICounterAppModel>(new CounterAppModel());

            // 注册存储工具的对象
            this.RegisterUtility<IStorage>(new Storage());

            // 注册 System
            this.RegisterSystem<IAchievementSystem>(new AchievementSystem());
        }
    }

}