using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace QFramework.Example
{
    public class QueryExampleApp : Architecture<QueryExampleApp>
    {
        protected override void Init()
        {
            this.RegisterModel(new StudentModel());
            this.RegisterModel(new TeacherModel());
        }
    }
}